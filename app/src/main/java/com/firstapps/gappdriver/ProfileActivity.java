package com.firstapps.gappdriver;

import android.os.Bundle;

import com.firstapps.gappdriver.library.BaseActivity;

import butterknife.ButterKnife;

/**
 * Created by ICortes on 12/06/17.
 */

public class ProfileActivity extends BaseActivity{

    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setTitle("Mi perfil");
        setContentView(R.layout.activity_profile);
        ButterKnife.bind(this);
    }
}
