package com.firstapps.gappdriver;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;

import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.ImageView;

import com.bumptech.glide.Glide;
import com.firstapps.gappdriver.library.BaseActivity;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import jp.wasabeef.glide.transformations.BlurTransformation;
import jp.wasabeef.glide.transformations.CropSquareTransformation;

/**
 * Created by ICortes on 26/05/17.
 */

public class MainActivity extends BaseActivity {
    @BindView(R.id.act_main_drawer_layout)
    DrawerLayout mDrawerLayout;
    @BindView(R.id.toolbar)
    Toolbar mToolbar;
    View section1;


    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setToolbarEnabled(false);
        setContentView(R.layout.activity_main);
        ButterKnife.bind(this);
        setupDrawer();


        Glide.with(this).load(R.drawable.icon_test_background)
                .bitmapTransform(new BlurTransformation(this, 14), new CropSquareTransformation(this))
                .into((ImageView) findViewById(R.id.htab_header));

        section1 = findViewById(R.id.section1);


        View header1 = findViewById(R.id.act_main_my_requisition);
        header1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (section1.getVisibility() == View.GONE) {
                    section1.setVisibility(View.VISIBLE);
                } else {
                    section1.setVisibility(View.GONE);
                }
            }
        });
    }

    private void setupDrawer() {
        mToolbar.setNavigationIcon(R.drawable.ic_menu_white);
        mToolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (mDrawerLayout.isDrawerOpen(GravityCompat.START))
                    mDrawerLayout.closeDrawer(GravityCompat.START);
                else
                    mDrawerLayout.openDrawer(GravityCompat.START);
            }
        });

        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(this, mDrawerLayout,
                R.string.drawer_open, R.string.drawer_close);
        toggle.syncState();
        mDrawerLayout.addDrawerListener(toggle);
    }

    @OnClick(R.id.act_main_my_profile)
    public void onClickProfile(View v) {
        startActivity(new Intent(this, ProfileActivity.class));
    }

    @OnClick(R.id.act_main_new_requisition)
    public void onClickNewRequisition(View v) {
        startActivity(new Intent(this, NewCollectionActivity.class));
    }

    @OnClick(R.id.act_main_account_status)
    public void onClickAccountStatus(View v) {
        startActivity(new Intent(this, AccountStatusActivity.class));
    }

}
