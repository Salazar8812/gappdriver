package com.firstapps.gappdriver;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import com.firstapps.gappdriver.library.BaseActivity;

import butterknife.ButterKnife;

public class LoginActivity extends BaseActivity implements View.OnClickListener {
    Button createButton;
    Button ingresarButton;
    TextView recuperarTextView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setToolbarEnabled(false);
        setContentView(R.layout.activity_login);
        ButterKnife.bind(this);

        createButton=(Button)findViewById(R.id.createButton);
        ingresarButton=(Button)findViewById(R.id.ingresarButton);
        recuperarTextView=(TextView)findViewById(R.id.recuperarTextView);

        createButton.setOnClickListener(this);
        ingresarButton.setOnClickListener(this);
        recuperarTextView.setOnClickListener(this);
    }


    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.ingresarButton:
                startActivity(new Intent(LoginActivity.this,MainActivity.class));
                break;
            default:
                break;
        }
    }
}
