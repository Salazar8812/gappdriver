package com.firstapps.gappdriver;

import android.os.Bundle;
import android.view.View;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;

import com.firstapps.gappdriver.library.BaseActivity;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

/**
 * Created by ICortes on 12/06/17.
 */

public class NewCollectionActivity extends BaseActivity {

    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setTitle("Nueva recolección");
        setContentView(R.layout.activity_new_collection);
        ButterKnife.bind(this);


    }

}
